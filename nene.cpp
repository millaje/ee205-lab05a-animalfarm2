///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   16 Feb 2021 
////////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "nene.hpp"


using namespace std;

namespace animalfarm {

Nene::Nene( string newID, enum Color newColor, enum Gender newGender ) {
   gender = newGender;         /// Get from the constructor... not all fishs are the same gender (this is a has-a relationship)
   species = "Branta sandvicensis";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
   featherColor = newColor;       /// A has-a relationship, so it comes through the constructor

     isMigratory = 1;       /// An is-a relationship, so it's safe to hardcode.  All fishs have the same gestation period.

   id = newID;
}


const string Nene::speak() {
   return string( "Nay, nay" );
}
/// Print our Fish and name first... then print whatever information Mammal holds.
void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << id << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm




